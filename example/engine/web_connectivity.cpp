// Public domain 2017, Simone Basso <bassosimone@gmail.com.

#include "test/winsock.hpp"

#define MK_ENGINE_INTERNALS
#include <measurement_kit/engine.h>

#include <iostream>

#include <measurement_kit/common/nlohmann/json.hpp>

int main() {
    nlohmann::json settings{
        {"inputs", {
            "https://www.google.com/",
            "https://ooni.torproject.org/",
        }},
        {"name", "WebConnectivity"},
        {"log_level", "INFO"},
    };
    std::clog << settings.dump() << "\n";
    mk::engine::Task task{std::move(settings)};
    while (!task.is_done()) {
        auto event = task.wait_for_next_event();
        std::clog << event << "\n";
    }
}
